//
//  ViewController.swift
//  coffeMachine
//
//  Created by Bogdan Kravchenko on 16.10.2018.
//  Copyright © 2018 Kravchenko Group. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let coffee = coffeMachine(tankMilk: 0, tankWater: 0, tankBeans: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
   statusTank()
//        makeAmericano()
        ingridients()
        makeAmericano()
        statusTank()
        
    }
    func statusTank(){
        coffee.statusTank()
    }
    
    func ingridients() {
//        coffee.addBeans()
//        coffee.addBeans()
//         coffee.addBeans()
//        coffee.addWater()
//            coffee.addWater()
    }
    
    func makeAmericano(){
        coffee.americano()
  }
    
    func makeCappuccino() {
        coffee.cappuccino()
    }
    
    func makeEspresso() {
        coffee.espresso()
    }
    
    func makeLatte() {
        coffee.latte()
    }
    
}

